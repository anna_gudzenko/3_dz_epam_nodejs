const {Schema, model} = require('mongoose');

const LoadSchema = new Schema({
  created_by: String,
  assigned_to: String,
  status: {
    type: String,
    enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
    default: 'NEW',
  },
  state: {
    type: String,
    enum: [
      'En route to Pick Up',
      'Arrived to Pick Up',
      'En route to delivery',
      'Arrived to delivery',
    ],
    default: 'En route to Pick Up',
  },
  name: String,
  payload: Number,
  pickup_address: String,
  delivery_address: String,

  dimensions: {
    width: Number,
    length: Number,
    height: Number,
  },
  logs: [
    {
      message: String,
      time: Date,
    },
  ],
  created_date: Date,
});

module.exports = model('Load', LoadSchema);
