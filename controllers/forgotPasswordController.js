const User = require('../models/user');
const sendEmail = require('../services/sendEmail');
const Joi = require('joi');
const generatePassword = require('../helpers/generatePass');

const forgotPassword = async (req, res) => {
  try {
    const schema = Joi.object({email: Joi.string().email().required()});
    const {error} = schema.validate(req.body);
    if (error) {
      res.status(400).json({message: error.details[0].message});
    }
    const user = await User.findOne({email: req.body.email});
    if (!user) {
      res.status(400).json({
        message: `user with given email ${req.body.email} does not exists`,
      });
    } else {
      await sendEmail(
          req.body.email,
          'password reset',
          `New password is: ${generatePassword(10)}`,
      );
      res
          .status(200)
          .json({message: 'New password sent to your email address'});
    }
  } catch (error) {
    res.status(500).json({
      message: 'Exeption in services passwordReset: ' + error.message,
    });
    console.log('Exeption in services passwordReset: ' + error.message);
  }
};

module.exports = {forgotPassword};
