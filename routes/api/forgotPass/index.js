const {Router} = require('express');
const router = new Router();
const {
  forgotPassword,
} = require('../../../controllers/forgotPasswordController');

router.post('/', forgotPassword);

module.exports = router;
