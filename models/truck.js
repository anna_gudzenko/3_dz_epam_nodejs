const {Schema, model} = require('mongoose');

const TruckSchema = new Schema({
  created_by: String,
  assigned_to: String,
  type: {
    type: String,
    enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
    // default: 'SPRINTER'
  },
  status: {
    type: String,
    enum: ['Ol', 'IS'],
    default: 'IS',
  },
  created_date: Date,
});

module.exports = model('Truck', TruckSchema);
