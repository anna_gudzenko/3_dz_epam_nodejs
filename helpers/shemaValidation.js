const Joi = require('joi');

const registerValidation = (data) => {
  const regValidSh = Joi.object({
    email: Joi.string().email().required(),
    // username: Joi.string().min(3).max(30).required(),
    password: Joi.string()
        .required()
        .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),
    role: Joi.string().required(),
  }).unknown();
  return regValidSh.validate(data);
};

const loginValidation = (data) => {
  const regValidSh = Joi.object({
    email: Joi.string().email().required(),
    password: Joi.string()
        .required()
        .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),
  }).unknown();
  return regValidSh.validate(data);
};

const passwordValidation = (data) => {
  const regValidSh = Joi.object({
    newPassword: Joi.string()
        .required()
        .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),
  }).unknown();
  return regValidSh.validate(data);
};

const trucksValidation = (data) => {
  const regValidSh = Joi.object({
    type: Joi.string().required(),
  }).unknown();
  return regValidSh.validate(data);
};

const loadsValidation = (data) => {
  const regValidSh = Joi.object({
    name: Joi.string().required(),
    payload: Joi.number().required(),
    pickup_address: Joi.string().required(),
    delivery_address: Joi.string().required(),
    dimensions: {
      width: Joi.number().required(),
      length: Joi.number().required(),
      height: Joi.number().required(),
    },
  }).unknown();
  return regValidSh.validate(data);
};

module.exports = {
  registerValidation,
  loginValidation,
  passwordValidation,
  trucksValidation,
  loadsValidation,
};
