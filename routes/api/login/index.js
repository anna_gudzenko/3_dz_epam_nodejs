const {Router} = require('express');
const router = new Router();
const {handleLogin} = require('../../../controllers/loginController');

router.post('/', handleLogin);

module.exports = router;
