const {Router} = require('express');
const router = new Router();
const authMiddleware = require('../../../middleware/authMiddleware');

const {
  getUserById,
  deleteUser,
  updateUser,
} = require('../../../controllers/userControllers');

router
    .get('/', authMiddleware, getUserById)
    .delete('/', authMiddleware, deleteUser)
    .patch('/', authMiddleware, updateUser);

module.exports = router;
