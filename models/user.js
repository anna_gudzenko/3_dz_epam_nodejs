const {Schema, model} = require('mongoose');
const User = new Schema({
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    enum: ['DRIVER', 'SHIPPER'],
  },
  created_date: Date,
});

module.exports = model('User', User);
