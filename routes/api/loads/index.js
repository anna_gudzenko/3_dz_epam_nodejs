const {Router} = require('express');
const router = new Router();

const {
  postLoad,
  getLoads,
  updateActiveState,
  getLoadById,
  updateUsersLoadById,
  deleteLoad,
  postUsersLoadById,
} = require('../../../controllers/loadsControllers');

router
    .post('/', postLoad)
    .get('/', getLoads)
    .patch('/', updateActiveState)
    .get('/:id', getLoadById)
    .put('/:id', updateUsersLoadById)
    .delete('/:id', deleteLoad)
    .post('/:id/post', postUsersLoadById);

module.exports = router;
