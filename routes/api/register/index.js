const {Router} = require('express');
const router = new Router();
const {registerNewUser} = require('../../../controllers/registerController');

router.post('/', registerNewUser);

module.exports = router;
