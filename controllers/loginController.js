// const CredentialsSchema = require("../models/credentials");
const User = require("../models/user");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { loginValidation } = require("../helpers/shemaValidation");

const handleLogin = async (req, res) => {
  const { email, password } = req.body;
  const { error } = loginValidation(req.body);
  if (error) {
    return res.status(400).json({ message: error.details[0].message });
  }

  try {
    const foundUser = await User.findOne({
      email: email,
    }).exec();
    if (!foundUser) {
      return res.status(400).json({
        message: `User ${email} Unauthorized and not found in system`,
      });
    }
    // evaluate password
    const match = await bcrypt.compare(password, foundUser.password);
    if (match) {
      const accsessToken = jwt.sign(
        {
          id: foundUser._id,
        },
        process.env.ACCESS_TOKEN_SECRET
      );

      res.status(200).json({ message: "Success", jwt_token: accsessToken });
    } else {
      return res
        .status(401)
        .json({ message: `User ${email} Unauthorized or wrong password` });
    }
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
};

module.exports = { handleLogin };
