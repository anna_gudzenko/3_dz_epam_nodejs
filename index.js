require("dotenv").config();
const express = require("express");
const mongoose = require("mongoose");
const connectionDB = require("./config/dbConn");
const { logger } = require("./middleware/logevents");

// ConfigService.init();
const app = express();
app.use(logger);
app.use(express.json());

app.use("/api/auth/register", require("./routes/api/register"));
app.use("/api/auth/login", require("./routes/api/login"));
app.use("/api/auth/forgot_password", require("./routes/api/forgotPass"));
//
app.use("/api/users/me", require("./routes/api/users"));
app.use("/api/users/me/password", require("./routes/api/users"));

app.use("/api/trucks", require("./routes/api/trucks"));

app.use("/api/loads", require("./routes/api/loads"));
app.use("/api/loads", require("./routes/api/loads/activeLoads"));
app.use("/api/loads/active", require("./routes/api/loads/activeLoads"));
app.use("/api/loads/active/state", require("./routes/api/loads"));

connectionDB();

const port = process.env.APP_PORT || 8080;
mongoose.connection.once("open", () => {
  app.listen(port, () => {
    console.log(`App start on port ${port}`);
  });
});
