const LoadSchema = require('../models/load');
const TruckSchema = require('../models/truck');
const getUserId = require('../helpers/getUserId');
const User = require('../models/user');
const {loadsValidation} = require('../helpers/shemaValidation');

const postLoad = async (req, res) => {
  try {
    const ID = getUserId(req);
    const {error} = loadsValidation(req.body);
    if (error) {
      return res.status(400).json({message: error.details[0].message});
    }
    const user = await User.findById(ID).exec();
    if (user.role !== 'SHIPPER') {
      res.status(400).json({message: 'Only Shipper can create the Loads'});
    } else {
      await LoadSchema.create({
        created_by: ID,
        assigned_to: null,
        status: 'POSTED',
        name: req.body.name,
        payload: req.body.payload,
        pickup_address: req.body.pickup_address,
        delivery_address: req.body.delivery_address,
        dimensions: {
          width: req.body.dimensions.width,
          length: req.body.dimensions.length,
          height: req.body.dimensions.height,
        },
        logs: [
          {
            message: `Load assigned to driver with id ${ID}`,
            time: new Date(),
          },
        ],
        created_date: new Date(),
      });

      res.status(200).json({message: 'Load created successfully'});
    }
  } catch (error) {
    res.status(500).json({message: error.message});
  }
};

const getLoads = async (req, res) => {
  try {
    const status = req.query.status;
    const limit = req.query.limit;
    const offset = req.query.offset ? req.query.offset : 0;
    const ID = getUserId(req);
    const loads = await LoadSchema.find().exec();
    if (!loads) {
      res.status(400).json({message: 'Loads not found'});
    }
    const user = await User.findById(ID).exec();
    if (!user) {
      res.status(400).json({message: 'Unauthorized user cant see loads'});
    }
    if (user.role === 'DRIVER') {
      let completedActiveLoads;
      if (status) {
        completedActiveLoads = loads.filter(
            (e) => e.status === status.toUpperCase(),
        );
      } else {
        completedActiveLoads = loads.filter((e) =>
          e.status === 'ASSIGNED' || e.status === 'SHIPPED' ? e : null,
        );
      }
      if (offset) {
        completedActiveLoads.splice(offset, completedActiveLoads.length);
      }
      if (limit) {
        completedActiveLoads.splice(limit);
      }

      res.status(200).json({loads: completedActiveLoads});
    } else if (user.role === 'SHIPPER') {
      res.status(200).json({loads});
    }
  } catch (error) {
    res.status(500).json({message: error.message});
  }
};

const getActiveLoads = async (req, res) => {
  try {
    const ID = getUserId(req);
    const loads = await LoadSchema.find().exec();
    if (!loads) {
      res.status(400).json({message: 'Loads not found'});
    }
    const user = await User.findById(ID).exec();
    if (!user) {
      res.status(400).json({message: 'Unauthorized user cant see loads'});
    }
    if (user.role !== 'DRIVER') {
      res.status(400).json({message: 'Available Only for Driver'});
    } else {
      const assignedLoad = loads.find((l) => l.status === 'ASSIGNED');

      res.status(200).json({load: assignedLoad});
    }
  } catch (error) {
    res.status(500).json({message: error.message});
  }
};

const updateActiveState = async (req, res) => {
  try {
    const ID = getUserId(req);
    const loads = await LoadSchema.find().exec();
    if (!loads) {
      res.status(400).json({message: 'Loads not found'});
    }
    const user = await User.findById(ID).exec();
    if (!user) {
      res.status(400).json({message: 'Unauthorized user cant see loads'});
    }
    if (user.role !== 'DRIVER') {
      res.status(400).json({message: 'Available Only for Driver'});
    } else {
      const assignedLoad = loads.find(
          (l) => l.status === 'ASSIGNED' && l.assigned_to === ID,
      );

      if (!assignedLoad) {
        res.status(400).json({message: 'you have not assigned loads'});
      } else {
        if (assignedLoad.state === 'En route to Pick Up') {
          await LoadSchema.findByIdAndUpdate(assignedLoad._id, {
            state: 'Arrived to Pick Up',
          }).exec();
          res
              .status(200)
              .json({message: 'Load state changed to "Arrived to Pick Up"'});
        } else if (assignedLoad.state === 'Arrived to Pick Up') {
          await LoadSchema.findByIdAndUpdate(assignedLoad._id, {
            state: 'En route to delivery',
          }).exec();
          res
              .status(200)
              .json({message: 'Load state changed to "En route to delivery"'});
        } else if (assignedLoad.state === 'En route to delivery') {
          await LoadSchema.findByIdAndUpdate(assignedLoad._id, {
            state: 'Arrived to delivery',
            status: 'SHIPPED',
          }).exec();
          res
              .status(200)
              .json({message: 'Load state changed to "Arrived to delivery"'});
        }
      }
    }
  } catch (error) {
    res.status(500).json({message: error.message});
  }
};

const getLoadById = async (req, res) => {
  try {
    if (!req.params.id) {
      res.status(400).json({message: 'No id in params'});
    }
    const load = await LoadSchema.findById(req.params.id).exec();
    if (!load) {
      res.status(400).json({message: 'Load not found'});
    }
    res.status(200).json({load});
  } catch (error) {
    res.status(500).json({message: error.message});
  }
};

const updateUsersLoadById = async (req, res) => {
  try {
    const ID = getUserId(req);
    if (!req.params.id) {
      res.status(400).json({message: 'No id in params'});
    }
    const load = await LoadSchema.find({_id: req.params.id}).exec();

    if (!load) {
      res.status(400).json({message: 'Loads not found'});
    }
    const user = await User.findById(ID).exec();
    if (!user) {
      res.status(400).json({message: 'Unauthorized user cant see loads'});
    }
    if (user.role === 'DRIVER') {
      res.status(400).json({message: 'Available Only for Snipper'});
    } else {
      await LoadSchema.findByIdAndUpdate(req.params.id, req.body).exec();
      res.status(200).json({message: 'Load details changed successfully'});
    }
  } catch (error) {
    res.status(500).json({message: error.message});
  }
};

const deleteLoad = async (req, res) => {
  try {
    const ID = getUserId(req);
    if (!req.params.id) {
      res.status(400).json({message: 'No id in params'});
    }
    const load = await LoadSchema.findById(req.params.id).exec();
    if (!load) {
      res.status(400).json({message: 'Load not found'});
    }
    const user = await User.findById(ID).exec();
    if (user.role === 'DRIVER') {
      res.status(400).json({message: 'Only Snipper can remove the Loads'});
    } else {
      await LoadSchema.findByIdAndDelete(req.params.id).exec();
      res.status(200).send({message: 'Load deleted successfully'});
    }
  } catch (error) {
    res.status(500).send({message: error.message});
  }
};

const postUsersLoadById = async (req, res) => {
  const ID = getUserId(req);
  try {
    if (!req.params.id) {
      res.status(400).json({message: 'Bad request no id in params'});
    }

    const user = await User.findById(ID).exec();
    if (!user) {
      res.status(400).json({message: 'Unauthorized user cant see loads'});
    }
    if (user.role === 'DRIVER') {
      res.status(400).json({message: 'Available Only for Shnipper'});
    } else {
      const truck = await TruckSchema.find().exec();
      if (!truck) {
        res.status(400).json({message: 'Truck not found'});
      }
      const assignedTruck = truck.find(
          (t) => t.assigned_to !== null && t.status === 'IS',
      );

      if (!assignedTruck) {
        res.status(400).json({message: 'Truck not found'});
      } else {
        await TruckSchema.findByIdAndUpdate(assignedTruck._id, {
          status: 'ON',
        }).exec();

        const load = await LoadSchema.findById(req.params.id).exec();
        if (!load) {
          res.status(400).json({message: 'Load not found'});
        }

        await LoadSchema.findByIdAndUpdate(req.params.id, {
          assigned_to: assignedTruck.assigned_to,
          status: 'ASSIGNED',
          logs: [
            {
              message: `Load assigned to driver with id ${
                assignedTruck.assigned_to}`,
              time: new Date(),
            },
          ],
        }).exec();

        res.status(200).json({
          message: 'Load posted successfully',
          driver_found: true,
        });
      }
    }
  } catch (error) {
    res.status(500).json({message: error.message});
  }
};

const getUsersLoadShippingInfo = async (req, res) => {
  try {
    const ID = getUserId(req);
    if (!req.params.id) {
      res.status(400).json({message: 'No id in params'});
    }

    const user = await User.findById(ID).exec();
    if (!user) {
      res.status(400).json({message: 'Unauthorized user cant see loads'});
    }
    if (user.role === 'DRIVER') {
      res.status(400).json({message: 'Available Only for Snipper'});
    } else {
      const load = await LoadSchema.find({_id: req.params.id}).exec();
      if (!load) {
        res.status(400).json({message: 'Loads not found'});
      }
      if (load[0].status !== 'ASSIGNED') {
        res.status(400).json({message: 'Your haven\'t active Loads'});
      } else {
        res.status(200).json({load});
      }
    }
  } catch (error) {
    res.status(500).json({message: error.message});
  }
};

module.exports = {
  postLoad,
  getLoads,
  getActiveLoads,
  updateActiveState,
  getLoadById,
  updateUsersLoadById,
  deleteLoad,
  postUsersLoadById,
  getUsersLoadShippingInfo,
};
