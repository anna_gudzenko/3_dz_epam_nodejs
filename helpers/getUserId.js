const jwt = require('jsonwebtoken');

module.exports = function(req, res) {
  if (req.headers.authorization) {
    const token = req.headers.authorization.split(' ')[1];
    if (!token) {
      throw new Error({message: 'User token is not correct'});
    }
    const decodedData = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET);
    return decodedData.id;
  } else {
    throw new Error({message: 'User is not authorized'});
  }
};
