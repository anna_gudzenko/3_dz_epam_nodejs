const TruckSchema = require('../models/truck');
const getUserId = require('../helpers/getUserId');
const User = require('../models/user');
const {trucksValidation} = require('../helpers/shemaValidation');

const postTruck = async (req, res) => {
  try {
    const ID = getUserId(req);

    const {error} = trucksValidation(req.body);
    if (error) {
      return res.status(400).json({message: error.details[0].message});
    }

    const user = await User.findById(ID).exec();
    if (user.role !== 'DRIVER') {
      res.status(400).json({message: 'Only Driver can create the Truks'});
    } else {
      await TruckSchema.create({
        created_by: ID,
        assigned_to: null,
        type: req.body.type,
        created_date: new Date(),
      });

      res.status(200).json({message: 'Truck created successfully'});
    }
  } catch (error) {
    res.status(500).json({message: error.message});
  }
};

const getTrucks = async (req, res) => {
  const ID = getUserId(req);
  try {
    const trucks = await TruckSchema.find({
      created_by: ID,
    }).exec();

    if (!trucks) {
      res.status(400).json({message: 'Notes not found'});
    }
    const user = await User.findById(ID).exec();
    if (user.role !== 'DRIVER') {
      res.status(400).json({message: 'Only Driver can create the Truks'});
    } else {
      res.status(200).json({trucks});
    }
  } catch (error) {
    res.status(500).json({message: error.message});
  }
};

const getTruckById = async (req, res) => {
  try {
    const ID = getUserId(req);
    const truck = await TruckSchema.findById(req.params.id).exec();

    if (!truck) {
      res.status(400).json({message: 'Note not found'});
    }
    const user = await User.findById(ID).exec();
    if (user.role !== 'DRIVER') {
      res.status(400).json({message: 'Only Driver can see the Truks'});
    } else {
      res.status(200).json({truck});
    }
  } catch (error) {
    res.status(500).json({message: error.message});
  }
};

const updateTruck = async (req, res) => {
  try {
    const ID = getUserId(req);
    const {error} = trucksValidation(req.body);
    if (error) {
      return res.status(400).json({message: error.details[0].message});
    }

    const user = await User.findById(ID).exec();
    if (user.role !== 'DRIVER') {
      res.status(400).json({message: 'Only Driver can update the Truks'});
    } else {
      await TruckSchema.findByIdAndUpdate(req.params.id, {
        type: req.body.type,
      }).exec();
      res.status(200).json({message: 'Truck details changed successfully'});
    }
  } catch (error) {
    res.status(500).json({message: error.message});
  }
};

const deleteTruck = async (req, res) => {
  try {
    const ID = getUserId(req);
    const truck = await TruckSchema.findById(req.params.id).exec();
    if (!truck) {
      res.status(400).json({message: 'Truck not found'});
    }
    const user = await User.findById(ID).exec();
    if (user.role !== 'DRIVER') {
      res.status(400).json({message: 'Only Driver can update the Truks'});
    } else {
      await TruckSchema.findByIdAndDelete(req.params.id).exec();
      res.status(200).send({message: 'Truck deleted successfully'});
    }
  } catch (error) {
    res.status(500).send({message: error.message});
  }
};

const updateAssignTruck = async (req, res) => {
  const ID = getUserId(req);
  try {
    if (!req.params.id) {
      res.status(400).json({message: 'Bad request no truck id in params'});
    }
    const user = await User.findById(ID).exec();
    if (user.role === 'DRIVER') {
      const truck = await TruckSchema.findById(req.params.id).exec();
      if (!truck) {
        res.status(400).json({message: 'Truck not found'});
      }
      const trucksAll = await TruckSchema.find().exec();

      if (!trucksAll.find((e) => e.assigned_to === ID)) {
        await TruckSchema.findByIdAndUpdate(req.params.id, {
          assigned_to: ID,
        }).exec();
        res.status(200).json({message: 'Truck assigned successfully'});
      } else {
        res.status(400).json({message: 'Truck ia already assigned'});
      }
    } else {
      res.status(400).json({message: 'Only Driver assigned trucks'});
    }
  } catch (error) {
    res.status(500).json({message: error.message});
  }
};

module.exports = {
  postTruck,
  getTrucks,
  getTruckById,
  updateTruck,
  deleteTruck,
  updateAssignTruck,
};
