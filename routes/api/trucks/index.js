const {Router} = require('express');
const router = new Router();
const authMiddleware = require('../../../middleware/authMiddleware');

const {
  postTruck,
  getTrucks,
  getTruckById,
  updateTruck,
  deleteTruck,
  updateAssignTruck,
} = require('../../../controllers/trucksControllers');

router
    .post('/', authMiddleware, postTruck)
    .get('/', authMiddleware, getTrucks)
    .get('/:id', authMiddleware, getTruckById)
    .put('/:id', authMiddleware, updateTruck)
    .delete('/:id', authMiddleware, deleteTruck)
    .post('/:id/assign', authMiddleware, updateAssignTruck);

module.exports = router;
