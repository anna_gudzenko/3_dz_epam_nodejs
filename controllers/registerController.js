const User = require('../models/user');
const bcrypt = require('bcrypt');
const {registerValidation} = require('../helpers/shemaValidation');

const registerNewUser = async (req, res) => {
  const {email, password, role} = req.body;
  const {error} = registerValidation(req.body);
  if (error) {
    return res.status(400).json({message: error.details[0].message});
  }
  // check for duplicate usernames in db
  const duplicate = await User.findOne({
    email: email,
  }).exec();
  if (duplicate) {
    return res.status(400).json({message: `User: ${email} already exists`});
  }
  try {
    // encrypt the password
    const hashedPwd = await bcrypt.hash(password, 10);
    // create and store the new user
    await User.create({
      email: email,
      password: hashedPwd,
      role: role,
    });

    res.status(200).json({message: 'Profile created successfully'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

module.exports = {registerNewUser};
