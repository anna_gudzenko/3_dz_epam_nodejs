const {Router} = require('express');
const router = new Router();

const {
  getActiveLoads,
  getUsersLoadShippingInfo,
} = require('../../../controllers/loadsControllers');

router
    .get('/', getActiveLoads)
    .get('/:id/shipping_info', getUsersLoadShippingInfo);
module.exports = router;
